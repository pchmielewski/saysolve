﻿using System;
using System.Linq;
using Akka.Actor;
using SaySolve.Chat.Client.ConsoleApp.Actors;
using SaySolve.Data.Messages;
using SaySolve.Data.Services;
using SaySolve.Data.Services.Interfaces;

namespace SaySolve.Chat.Client.ConsoleApp
{
    class Program
    {
        private static IUserRepository _repo;

        private static string _chatRoomActorPath;

        private static bool _loggedInRoom;

        static void Main(string[] args)
        {
            Console.WriteLine("CLIENT Started ");

            _repo = new UserRepository();

            using (var system = ActorSystem.Create("MyClient"))
            {
                string username;
                var chatClient = LogUser(system, out username);
                Console.WriteLine($"{username} user added");
                chatClient.Tell(new Connect());

                while (true)
                {
                    var input = Console.ReadLine();
                    if (input.StartsWith("/"))
                    {
                        var parts = input.Split(' ');
                        var cmd = parts[0];
                        
                        if (parts.Length <= 1)
                        {
                            Console.WriteLine("Add parameter to commend");
                        }
                        else
                        {
                            var firstParameter = parts[1];
                            var secondParameter = string.Join(" ", parts.Skip(2));

                            ExecuteCommand(cmd, chatClient, firstParameter, secondParameter);
                        }
                    }
                    else if (_loggedInRoom)
                    {
                        chatClient.Tell(new SendMessage(input, _chatRoomActorPath));
                    }
                    else
                    {
                        Console.WriteLine("Please login to chat");
                    }
                }
            }

        }

        private static void ExecuteCommand(string cmd, IActorRef chatClient, string firstParameter, string secondParameter)
        {
            if (cmd == "/openroom")
            {
                int input = int.Parse(firstParameter);
                var interlocutorId = _repo.GetUsers().ToList()[input].UserId;
                chatClient.Tell(new StartChatWithSomeone(interlocutorId));
                _loggedInRoom = true;
            }
            if (cmd == "/joinroom") // jest drobny problem. do poprawki
            {
                chatClient.Tell(new JoinChatRoom(firstParameter));
                _chatRoomActorPath = firstParameter;
                _loggedInRoom = true;
            }
            if (cmd == "/switchroom")
            {
                chatClient.Tell(new SwitchChatRoom(firstParameter));
                _chatRoomActorPath = firstParameter;
                _loggedInRoom = true;
            }
            else if (cmd == "/exit")
            {
                chatClient.Tell(new ExitChatRoom(firstParameter));
                _loggedInRoom = false; // to powinno by zalatwione aktorami
            }
        }

        private static IActorRef LogUser(ActorSystem system, out string logedUsername)
        {
            Console.WriteLine("Dodaj id uzytkownika. Tzn zaloguj sie");
            var input = Console.ReadLine();
            var listOfUsers = _repo.GetUsers().ToList();

            var user = listOfUsers[int.Parse(input)];
            logedUsername = user.Username;
            return system.ActorOf(Props.Create(() => new ChatClientActor(user)), "ChatClient_" + user.Username);
        }


    }
}
