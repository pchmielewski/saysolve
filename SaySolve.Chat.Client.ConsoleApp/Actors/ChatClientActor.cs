﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Akka.Actor;
using SaySolve.Data.Messages;
using SaySolve.Data.Models;

namespace SaySolve.Chat.Client.ConsoleApp.Actors
{
    // to mozna do innego projektu wyciagnac bo inni klienci tez beda mogli z tego kozystac
    public class ChatClientActor : ReceiveActor
    {
        private readonly ActorSelection _chatServer = Context.ActorSelection("akka.tcp://SaySolveChatServer@localhost:8081/user/ChatServer");

        public User User { get; private set; }

        public List<string> ChatRooms { get; private set; }

        public string CurrentChatRoomActorPath { get; private set; }

        public string ConsolePreFix { get; set; }

        public ChatClientActor(User user)
        {
            ChatRooms = new List<string>();
            ConsolePreFix = "> ";
            User = user;

            Receive<Connect>(message => HandleConnect(message));
            Receive<ConnectResponse>(message => HandleConnectResponse(message));

            Receive<Disconnect>(message => HandleDisconnect(message));
            Receive<DisconnectResponse>(message => HandleDisconnectResponse(message));
            

            Receive<StartChatWithSomeone>(message => HandleStartChatWithSomeone(message));
            Receive<InterlocutorNotConnected>(message => HandleInterlocutorNotConnected(message));
            Receive<StartChatWithSomeoneResponse>(message => HandleStartChatWithSomeoneResponse(message));

            Receive<SendMessage>(message => HandleSendMessage(message));
            Receive<SendMessageResponse>(message => HandleSendMessageResponse(message));


            Receive<SwitchChatRoom>(message => HandleSwitchChatRoom(message));
            Receive<ExitChatRoom>(message => HandleExitChatRoom(message));
            Receive<JoinChatRoom>(message => HandleJoinChatRoom(message));
        }

        private void HandleJoinChatRoom(JoinChatRoom message)
        {
            _chatServer.Tell(new JoinChatRoomRequest(message.ChatRoomActorPath, User));
        }

        private void HandleExitChatRoom(ExitChatRoom message)
        {
            ChatRooms.Remove(message.ChatRoomActorPath);
            _chatServer.Tell(new ExitChatRoomRequest(message.ChatRoomActorPath, User));
        }

        private void HandleSwitchChatRoom(SwitchChatRoom message)
        {
            CurrentChatRoomActorPath = message.ChatRoomActorPath;
            ConsolePreFix = message.ChatRoomActorPath +">";
            Console.WriteLine("Switched to: "+ message.ChatRoomActorPath);
        }

        private void HandleSendMessageResponse(SendMessageResponse message)
        {
            var mes = message.Username == User.Username
                ? "Me: " + message.Message
                : message.Username + ": " + message.Message;
            Console.WriteLine(ConsolePreFix + mes);
        }

        private void HandleSendMessage(SendMessage message)
        {
            _chatServer.Tell(new SendMessageRequest(User.UserId, CurrentChatRoomActorPath, message.Message));
        }

        private void HandleInterlocutorNotConnected(InterlocutorNotConnected message)
        {
            Console.WriteLine(ConsolePreFix + message.Message);
        }

        private void HandleDisconnectResponse(DisconnectResponse message)
        {
            Console.WriteLine(ConsolePreFix + message.Message);
        }

        private void HandleDisconnect(Disconnect message)
        {
            _chatServer.Tell(new DisconnectRequest(User.UserId, User.Username));
        }

        private void HandleConnectResponse(ConnectResponse message)
        {
            Console.WriteLine(ConsolePreFix + message.Message);
        }

        private void HandleConnect(Connect message)
        {
            Console.WriteLine("ChatClientActor->#ConnectRequest->ChatServerActor");
            _chatServer.Tell(new ConnectRequest(User));
        }

        private void HandleStartChatWithSomeone(StartChatWithSomeone message)
        {
            _chatServer.Tell(new StartChatWithSomeoneRequest(User.UserId, message.InterlocutorId));
        }

        private void HandleStartChatWithSomeoneResponse(StartChatWithSomeoneResponse message)
        {
            CurrentChatRoomActorPath = message.ChatRoomActorPath;
            ChatRooms.Add(message.ChatRoomActorPath);
            ConsolePreFix = message.ChatRoomActorPath + ">";
            Console.WriteLine(ConsolePreFix + message.Message);
        }
    }
}
