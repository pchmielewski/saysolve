﻿using Microsoft.Owin.Cors;
using Owin;

namespace SaySolve.Chat.Server
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseCors(CorsOptions.AllowAll);
            app.MapSignalR();
        }
    }
}
