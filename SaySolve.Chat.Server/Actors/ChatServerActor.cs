﻿using System;
using System.Collections.Generic;
using System.Linq;
using Akka.Actor;
using SaySolve.Chat.Server.Messages;
using SaySolve.Data.Messages;

namespace SaySolve.Chat.Server.Actors
{
    public class ChatServerActor : ReceiveActor
    {
        private readonly ActorSystem _chatSystem;
        private readonly HashSet<IActorRef> _chatRooms = new HashSet<IActorRef>();

        private readonly Dictionary<Guid,IActorRef> _allUsers = new Dictionary<Guid, IActorRef>();

        public ChatServerActor(ActorSystem chatSystem)
        {
            _chatSystem = chatSystem;

            Receive<ConnectRequest>(message => HandleConnectRequest(message));

            Receive<DisconnectRequest>(message => HandleDisconnectRequest(message));

            Receive<StartChatWithSomeoneRequest>(message => HandleStartChatWithSomeoneRequest(message));

            Receive<SendMessageRequest>(message => HandleSendMessageRequest(message));

            Receive<ExitChatRoomRequest>(message => HandleExitChatRoomRequest(message));

            Receive<JoinChatRoomRequest>(message => HandleJoinChatRoomRequest(message));

        }

        private void HandleJoinChatRoomRequest(JoinChatRoomRequest message)
        {
            var chatRoom = _chatRooms.FirstOrDefault(p => p.Path.ToString() == message.ChatRoomActorPath);
            chatRoom?.Tell(message, Sender);
        }

        private void HandleExitChatRoomRequest(ExitChatRoomRequest message)
        {
            var chatRoom = _chatRooms.FirstOrDefault(p => p.Path.ToString() == message.ChatRoomActorPath);
            chatRoom?.Tell(message);
        }

        private void HandleSendMessageRequest(SendMessageRequest message)
        {
            //trzeba dodac referencje do czatu
            var chatRoomActorReference = _chatSystem.ActorSelection(message.ChatRoomActorPath);
            chatRoomActorReference.Tell(message);
        }

        private void HandleDisconnectRequest(DisconnectRequest message)
        {
            _allUsers.Remove(message.UserId);
            Sender.Tell(new DisconnectResponse($"User {message.Username} was disconnected correctly"));
        }

        private void HandleConnectRequest(ConnectRequest message)
        {
            Console.WriteLine("ChatServerActor->#ConnectRequest->ChatClientActor");

            _allUsers.Add(message.User.UserId, Sender);

            // zwrocic informacje o niepoprawnym polaczeniu
            Sender.Tell(new ConnectResponse($"User {message.User.Username} was connected correctly"), Self);
        }

        private void HandleStartChatWithSomeoneRequest(StartChatWithSomeoneRequest message)
        {
            // zamienic na logowanie
            Console.WriteLine("ChatServerActor->#StartChatWithSomeoneRequest->ChatRoomActor");

            if (_allUsers.ContainsKey(message.InterlocutorId))
            {
                var newChatRoom = _chatSystem.ActorOf(Props.Create(() => new ChatRoomActor())); // pomyslec nad dodaniem nazwy aktora
                _chatRooms.Add(newChatRoom);

                newChatRoom.Tell(new AddChatRoomHost(message.UserId),Sender);

                var interlocutor = _allUsers.FirstOrDefault(p => p.Key == message.InterlocutorId).Value;

                if (interlocutor != null)
                {
                    newChatRoom.Tell(new AddInterlocutor(interlocutor, message.InterlocutorId), Sender); // senderem jest tutaj ChatClient
                }
                else
                {
                    Sender.Tell(new InterlocutorNotConnected($"User {message.InterlocutorId} curently is not connected"));
                }
                
            }
            else
            {
                // zamienic potem na nazwe 
                Sender.Tell(new InterlocutorNotConnected($"User {message.InterlocutorId} curently is not connected"));
            }
        }
    }
}