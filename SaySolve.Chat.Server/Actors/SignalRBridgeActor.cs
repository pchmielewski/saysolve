﻿using System.Threading.Tasks;
using Akka.Actor;
using SaySolve.Chat.Server.Hubs;
using SaySolve.Data.Messages;

namespace SaySolve.Chat.Server.Actors
{
    public class SignalRBridgeActor : ReceiveActor
    {
        private readonly IChatEventsPusher _chatEventsPusher;

        public SignalRBridgeActor()
        {
            _chatEventsPusher = new SignalRPushServices();

            Receive<Connect>(message => HandleConnect(message));
            Receive<ConnectResponse>(message => HandleConnectResponse(message));

            Receive<Disconnect>(message => HandleDisconnect(message));
            Receive<DisconnectResponse>(message => HandleDisconnectResponse(message));

            Receive<StartChatWithSomeone>(message => HandleStartChatWithSomeone(message));
            Receive<InterlocutorNotConnected>(message => HandleInterlocutorNotConnected(message));
            Receive<StartChatWithSomeoneResponse>(message => HandleStartChatWithSomeoneResponse(message));

            Receive<SendMessage>(message => HandleSendMessage(message));
            Receive<SendMessageResponse>(message => HandleSendMessageResponse(message));

            Receive<SwitchChatRoom>(message => HandleSwitchChatRoom(message));
            Receive<ExitChatRoom>(message => HandleExitChatRoom(message));
            Receive<JoinChatRoom>(message => HandleJoinChatRoom(message));
        }

        private void HandleJoinChatRoom(JoinChatRoom message)
        {
            throw new System.NotImplementedException();
        }

        private void HandleExitChatRoom(ExitChatRoom message)
        {
            throw new System.NotImplementedException();
        }

        private void HandleSwitchChatRoom(SwitchChatRoom message)
        {
            throw new System.NotImplementedException();
        }

        private void HandleSendMessageResponse(SendMessageResponse message)
        {
            throw new System.NotImplementedException();
        }

        private void HandleSendMessage(SendMessage message)
        {
            throw new System.NotImplementedException();
        }

        private void HandleStartChatWithSomeoneResponse(StartChatWithSomeoneResponse message)
        {
            throw new System.NotImplementedException();
        }

        private void HandleInterlocutorNotConnected(InterlocutorNotConnected message)
        {
            throw new System.NotImplementedException();
        }

        private void HandleStartChatWithSomeone(StartChatWithSomeone message)
        {
            _chatEventsPusher.AddPersonToChat();
        }

        private void HandleDisconnectResponse(DisconnectResponse message)
        {
            throw new System.NotImplementedException();
        }

        private void HandleDisconnect(Disconnect message)
        {
            throw new System.NotImplementedException();
        }

        private void HandleConnectResponse(ConnectResponse message)
        {
            throw new System.NotImplementedException();
        }

        private void HandleConnect(Connect message)
        {
            _chatEventsPusher.Connect();
        }
    }
}
