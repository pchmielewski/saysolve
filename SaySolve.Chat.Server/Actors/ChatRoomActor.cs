﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using SaySolve.Chat.Server.Messages;
using SaySolve.Data.Messages;
using SaySolve.Data.Models;
using SaySolve.Data.Services;
using SaySolve.Data.Services.Interfaces;

namespace SaySolve.Chat.Server.Actors
{
    public class ChatRoomActor : ReceiveActor
    {
        private readonly IUserRepository _userRepository;

        public string ChatRoomName { get; private set; }

        private readonly Dictionary<User,IActorRef> _users = new Dictionary<User, IActorRef>();

        public ChatRoomActor()
        {
            _userRepository = new UserRepository();
            Receive<AddChatRoomHost>(message => HandleAddChatRoomHost(message));
            Receive<AddInterlocutor>(message => HandleAddInterlocutor(message));
            Receive<StartChatWithSomeoneRequest>(message => HandleStartChatWithSomeoneRequest(message));
            Receive<SendMessageRequest>(message => HandleSendMessageRequest(message));
            Receive<ExitChatRoomRequest>(message => HandleExitChatRoomRequest(message));
            Receive<JoinChatRoomRequest>(message => HandleJoinChatRoomRequest(message));
        }

        private void HandleJoinChatRoomRequest(JoinChatRoomRequest message)
        {
            _users.Add(message.User, Sender);
            Sender.Tell(new JoinChatRoomResponse("You joined to: " + message.ChatRoomActorPath));
        }

        private void HandleExitChatRoomRequest(ExitChatRoomRequest message)
        {
            Console.WriteLine("Number of users before remove: "+ _users.Count);
            var leavingUser = _users.Keys.First(p => p.UserId == message.User.UserId);
            var leavingUserActorReference = _users[leavingUser];
            leavingUserActorReference.Tell(new SendMessageResponse("SYSTEM", "You leave room"), Self);
            _users.Remove(leavingUser);
            Console.WriteLine("Number of users after remove: " + _users.Count);
            foreach (var client in _users.Values) client.Tell(new SendMessageResponse("SYSTEM", message.User.Username + " leave room"), Self);
        }

        private void HandleSendMessageRequest(SendMessageRequest message)
        {
            var user = _users.Keys.FirstOrDefault(p => p.UserId == message.UserId);
            foreach (var client in _users.Values) client.Tell(new SendMessageResponse(user.Username, message.Message), Self);
        }

        private void HandleAddChatRoomHost(AddChatRoomHost message)
        {
            var host = _userRepository.GetUsers().FirstOrDefault(p => p.UserId == message.HostId);
            _users.Add(host, Sender);
            //tutaj trzeba dodac akcje w signalR ktora bedzie dynamicznie zmieniac nazwe czatu
            ChatRoomName = $"CHAT: " + string.Join(" ", _users.Keys.Select(p => p.Username));
            Sender.Tell(new StartChatWithSomeoneResponse("You started chat", Self.Path.ToString()));
        }

        private void HandleAddInterlocutor(AddInterlocutor message)
        {
            // to jest ograniczone narazie do jednego uzytkownika
            var interlocutor = _userRepository.GetUsers().FirstOrDefault(p => p.UserId == message.InterlocutorId);
            _users.Add(interlocutor, message.Interlocutor);

            ChatRoomName = $"CHAT: " + string.Join(" ", _users.Keys.Select(p=>p.Username)); // tutaj mozna spropagowac zmiane nazwy czatu
            message.Interlocutor.Tell(new StartChatWithSomeoneResponse("You started chat with: " + ChatRoomName, Self.Path.ToString()));
            //tutaj mozna dodac event ze ta osoba dolaczyla do czatu
        }

        private void HandleStartChatWithSomeoneRequest(StartChatWithSomeoneRequest message)
        {
           // _users.Add(Sender);
        }

        private void GetChatByName()
        {
            
        }
    }
}