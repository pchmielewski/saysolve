﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace SaySolve.Chat.Server.Hubs.Core
{
    // ten hub służy do monitorowania polaczeń clientow do reszty hubow 

    // wazna implemenrtacja, czytac z kodu tego i wzorowac sie 
    //https://github.com/JabbR/JabbR/blob/dev/JabbR/Hubs/Chat.cs

    //z tego tez ale mniej
    // https://github.com/NTaylorMullen/ShootR

    [HubName("monitor")]
    public class MonitorHub : Hub
    {
        public void NewEvent(string eventType, string connectionId)
        {
            Clients.All.newEvent(eventType, connectionId);
        }
    }
}
