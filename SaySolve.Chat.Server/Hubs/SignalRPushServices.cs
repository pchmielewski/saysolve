﻿using Microsoft.AspNet.SignalR;

namespace SaySolve.Chat.Server.Hubs
{
    public class SignalRPushServices : IChatEventsPusher
    {
        private static readonly IHubContext _chatHubContext;

        static SignalRPushServices()
        {
            _chatHubContext = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
        }

        public void ChatAdded()
        {
            throw new System.NotImplementedException();
        }

        public void UpdateMessages(string message)
        {
            _chatHubContext.Clients.All.updateMessages();
        }

        public void Connect()
        {
            _chatHubContext.Clients.All.connected();
        }

        public void AddPersonToChat()
        {
            _chatHubContext.Clients.All.addedToChat("Jacek");
        }
    }
}
