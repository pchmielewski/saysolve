﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.SignalR;
using SaySolve.Data.Messages;
using Akka.Actor;
using SaySolve.Chat.Server.Hubs.Core;

namespace SaySolve.Chat.Server.Hubs
{
    public class ChatHub : Hub
    {
        public void Connect()
        {
            ChatSystem.SignalRBridge.Tell(new Connect());

        }

        public void Disconnect()
        {
            Clients.All.disconnected();
        }

        public void AddMessageInChat(string message, string userId, string chatName)
        {
            ChatSystem.SignalRBridge.Tell(new SendMessage(message, chatName));
        }

        public void AddPersonChat(string interlocutorId, string chatName)
        {
            Guid tempGuid = Guid.NewGuid();

            if (int.Parse(interlocutorId)  == 0)
            {
                tempGuid =  new Guid("c88762c9-ff21-4734-9aa6-00e7ade6cb39");
            }

            ChatSystem.SignalRBridge.Tell(new StartChatWithSomeone(tempGuid));
        }

        public void AddedToChat()
        {
            Clients.All.addedToChat();
        }


        public override Task OnConnected()
        {
            SendMonitoringData("Connected", Context.ConnectionId);
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            SendMonitoringData("Disconnected", Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        public override Task OnReconnected()
        {
            SendMonitoringData("Reconnected", Context.ConnectionId);
            return base.OnReconnected();
        }

        // trzeba bedzie jeszcze dodac usera
        public void SendMonitoringData(string eventType, string connectionId)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<MonitorHub>();
            context.Clients.All.newEvent(eventType, connectionId);
        }
    }
}
