﻿namespace SaySolve.Chat.Server.Hubs
{
    public interface IChatEventsPusher
    {
        void ChatAdded();
        void UpdateMessages(string message);
        void Connect();
        void AddPersonToChat();
    }
}