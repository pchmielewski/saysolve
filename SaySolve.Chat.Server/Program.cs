﻿using System;
using Microsoft.Owin.Hosting;

namespace SaySolve.Chat.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MAIN SERVER");
            Console.WriteLine("--Akka chat running");
            ChatSystem.Create();

            RunSignalR();
            Console.ReadLine();

            ChatSystem.Shutdown();
        }

        private static void RunSignalR()
        {
            Console.WriteLine("--SignalR runing");
            string url = "http://localhost:9997";
            using (WebApp.Start(url))
            {
                Console.WriteLine("Server running on {0}", url);
                Console.ReadLine();
            }
        }
    }
}
