﻿using System;
using Akka.Actor;

namespace SaySolve.Chat.Server.Messages
{
    public class AddInterlocutor
    {
        public Guid InterlocutorId { get; private set; }
        public IActorRef Interlocutor { get; private set; }

        public AddInterlocutor(IActorRef interlocutor, Guid interlocutorId)
        {
            InterlocutorId = interlocutorId;
            Interlocutor = interlocutor;
        }
    }
}
