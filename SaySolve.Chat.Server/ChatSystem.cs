﻿using Akka.Actor;
using SaySolve.Chat.Server.Actors;

namespace SaySolve.Chat.Server
{
    public static class ChatSystem
    {
        private static ActorSystem _actorSystemInstance;
        public static IActorRef SignalRBridge { get; set; }

        public static void Create()
        {
            _actorSystemInstance = ActorSystem.Create("SaySolveChatServer");
            var chatActor = _actorSystemInstance.ActorOf(Props.Create(()=>new ChatServerActor(_actorSystemInstance)),"ChatServer");

            SignalRBridge = _actorSystemInstance.ActorOf(Props.Create(() => new SignalRBridgeActor()), "SignalRBridge");
        }

        public static void Shutdown()
        {
            _actorSystemInstance.Terminate();
            _actorSystemInstance.Dispose();
        }
    }
}
