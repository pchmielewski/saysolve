﻿using SaySolve.Data.Models;

namespace SaySolve.Data.Messages
{
    public class JoinChatRoomRequest
    {
        public string ChatRoomActorPath { get; private set; }
        public User User { get; private set; }

        public JoinChatRoomRequest(string chatRoomActorPath, User user)
        {
            ChatRoomActorPath = chatRoomActorPath;
            User = user;
        }
    }
}
