﻿using System;
using SaySolve.Data.Models;

namespace SaySolve.Data.Messages
{
    public class ExitChatRoomRequest
    {
        public string ChatRoomActorPath { get; private set; }
        public User User { get; private set; }

        public ExitChatRoomRequest(string chatRoomActorPath, User user)
        {
            ChatRoomActorPath = chatRoomActorPath;
            User = user;
        }
    }
}
