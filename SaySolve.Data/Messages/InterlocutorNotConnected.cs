﻿namespace SaySolve.Data.Messages
{
    public class InterlocutorNotConnected
    {
        public string Message { get; private set; }

        public InterlocutorNotConnected(string message)
        {
            Message = message;
        }
    }
}
