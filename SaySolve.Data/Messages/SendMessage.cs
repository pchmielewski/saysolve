﻿using System;

namespace SaySolve.Data.Messages
{
    public class SendMessage
    {
        public string Message { get; private set; }
        public string ChatName { get; private set; }


        public SendMessage(string message, string chatName)
        {
            Message = message;
            ChatName = chatName;
        }
    }
}
