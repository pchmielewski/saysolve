﻿using System;

namespace SaySolve.Data.Messages
{
    public class SendMessageRequest
    {
        public Guid UserId { get; private set; }
        public string ChatRoomActorPath { get; private set; }
        public string Message { get; private set; }

        public SendMessageRequest(Guid userId, string chatRoomActorPath, string message)
        {
            ChatRoomActorPath = chatRoomActorPath;
            UserId = userId;
            Message = message;
        }
    }
}
