﻿namespace SaySolve.Data.Messages
{
    public class SendMessageResponse
    {
        public string Username { get; private set; }
        public string Message { get; private set; }

        public SendMessageResponse(string username, string message)
        {
            Username = username;
            Message = message;
        }
    }
}