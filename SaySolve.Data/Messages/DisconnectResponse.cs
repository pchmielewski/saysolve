﻿namespace SaySolve.Data.Messages
{
    public class DisconnectResponse
    {
        public string Message { get; private set; }

        public DisconnectResponse(string message)
        {
            Message = message;
        }
    }
}