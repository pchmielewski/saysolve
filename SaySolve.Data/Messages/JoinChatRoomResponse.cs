﻿namespace SaySolve.Data.Messages
{
    public class JoinChatRoomResponse
    {
        public string Message { get; private set; }

        public JoinChatRoomResponse(string message)
        {
            Message = message;
        }
    }
}
