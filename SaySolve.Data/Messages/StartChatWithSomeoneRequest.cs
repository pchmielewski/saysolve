﻿using System;

namespace SaySolve.Data.Messages
{
    public class StartChatWithSomeoneRequest
    {
        public Guid UserId { get; private set; }
        public Guid InterlocutorId { get; private set; }

        public StartChatWithSomeoneRequest(Guid userId, Guid interlocutorId)
        {
            UserId = userId;
            InterlocutorId = interlocutorId;
        }
    }
}