﻿namespace SaySolve.Data.Messages
{
    public class JoinChatRoom
    {
        public string ChatRoomActorPath { get; private set; }

        public JoinChatRoom(string chatRoomActorPath)
        {
            ChatRoomActorPath = chatRoomActorPath;
        }
    }
}
