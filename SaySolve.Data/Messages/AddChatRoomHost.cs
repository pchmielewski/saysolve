﻿using System;

namespace SaySolve.Data.Messages
{
    public class AddChatRoomHost 
    {
        public Guid HostId { get; private set; }

        public AddChatRoomHost(Guid hostId)
        {
            HostId = hostId;
        }
    }
}
