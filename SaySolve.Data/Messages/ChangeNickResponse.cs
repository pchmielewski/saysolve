﻿namespace SaySolve.Data.Messages
{
    public class ChangeNickResponse
    {
        public string OldUsername { get; private set; }
        public string NewUsername { get; private set; }

        public ChangeNickResponse(string oldUsername, string newUsername)
        {
            OldUsername = oldUsername;
            NewUsername = newUsername;
        }
    }
}