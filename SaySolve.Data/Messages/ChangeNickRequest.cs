﻿namespace SaySolve.Data.Messages
{
    public class ChangeNickRequest
    {
        public string OldUsername { get; private set; }
        public string NewUsername { get; private set; }

        public ChangeNickRequest(string oldUsername, string newUsername)
        {
            OldUsername = oldUsername;
            NewUsername = newUsername;
        }
    }
}