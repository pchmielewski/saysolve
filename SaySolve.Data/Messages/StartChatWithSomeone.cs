﻿using System;

namespace SaySolve.Data.Messages
{
    public class StartChatWithSomeone
    {
        public Guid InterlocutorId { get; private set; }

        public StartChatWithSomeone(Guid interlocutorId)
        {
            InterlocutorId = interlocutorId;
        }
    }
}
