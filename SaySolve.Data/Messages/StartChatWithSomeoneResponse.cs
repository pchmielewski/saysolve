﻿using System;

namespace SaySolve.Data.Messages
{
    public class StartChatWithSomeoneResponse
    {
        public string Message { get; private set; }
        public string ChatRoomActorPath { get; private set; }


        public StartChatWithSomeoneResponse(string message, string chatRoomActorPath)
        {
            Message = message;
            ChatRoomActorPath = chatRoomActorPath;
        }
    }
}
