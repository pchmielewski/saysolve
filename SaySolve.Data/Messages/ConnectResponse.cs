﻿namespace SaySolve.Data.Messages
{
    public class ConnectResponse
    {
        public string Message { get; private set; }

        public ConnectResponse(string message)
        {
            Message = message;
        }
    }
}