﻿using System;

namespace SaySolve.Data.Messages
{
    public class DisconnectRequest
    {
        public Guid UserId { get; private set; }
        public string Username { get; private set; }

        public DisconnectRequest(Guid userId, string username)
        {
            Username = username;
            UserId = userId;
        }
    }
}