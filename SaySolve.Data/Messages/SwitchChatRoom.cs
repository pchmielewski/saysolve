﻿namespace SaySolve.Data.Messages
{
    public class SwitchChatRoom
    {
        public string ChatRoomActorPath { get; private set; }

        public SwitchChatRoom(string chatRoomActorPath)
        {
            ChatRoomActorPath = chatRoomActorPath;
        }
    }
}
