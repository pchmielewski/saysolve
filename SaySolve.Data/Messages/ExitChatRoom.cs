﻿namespace SaySolve.Data.Messages
{
    public class ExitChatRoom
    {
        public string ChatRoomActorPath { get; private set; }

        public ExitChatRoom(string chatRoomActorPath)
        {
            ChatRoomActorPath = chatRoomActorPath;
        }
    }
}
