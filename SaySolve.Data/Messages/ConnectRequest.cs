﻿using SaySolve.Data.Models;

namespace SaySolve.Data.Messages
{
    public class ConnectRequest
    {
        public User User { get; private set; }

        public ConnectRequest(User user)
        {
            User = user;
        }
    }
}