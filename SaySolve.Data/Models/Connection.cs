﻿using System;

namespace SaySolve.Data.Models
{
    public class Connection
    {
        public Guid Id { get; set; }
        public Guid ConnectionId { get; set; }
        public bool IsActive { get; set; }

        public User User { get; set; }
    }
}
