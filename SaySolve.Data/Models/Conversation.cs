﻿using System;

namespace SaySolve.Data.Models
{
    public class Conversation
    {
        public Guid Id { get; set; }
        public DateTime SendDate { get; set; }
        public string Message { get; set; }

        public User FirstUser { get; set; }
        public User SecondUser { get; set; }
    }
}
