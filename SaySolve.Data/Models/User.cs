﻿using System;

namespace SaySolve.Data.Models
{
    public class User
    {
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}
