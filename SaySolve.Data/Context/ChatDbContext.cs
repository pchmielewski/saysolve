﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using SaySolve.Data.Models;

namespace SaySolve.Data.Context
{
    class ChatDbContext : DbContext
    {
        public ChatDbContext() : base("name=SaySolve")
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Connection> Connections { get; set; }
        public DbSet<Conversation> Conversations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.HasDefaultSchema("CustomerService");

            modelBuilder.Entity<User>().HasKey(x => x.UserId);
            modelBuilder.Entity<User>().Property(x => x.UserId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Connection>().HasKey(x => x.Id);
            modelBuilder.Entity<Connection>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Conversation>().HasKey(x => x.Id);
            modelBuilder.Entity<Conversation>().Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            base.OnModelCreating(modelBuilder);
        }
    }
}
