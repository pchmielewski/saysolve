﻿using System;
using System.Collections.Generic;
using System.Linq;
using SaySolve.Data.Context;
using SaySolve.Data.Models;
using SaySolve.Data.Services.Interfaces;

namespace SaySolve.Data.Services
{
    public class UserRepository : IUserRepository
    {
        private readonly ChatDbContext _context = new ChatDbContext();

        public void AddUser(User user)
        {
            _context.Users.Add(user);
            _context.SaveChanges();
        }

        public IEnumerable<User> GetUsers()
        {
            return _context.Users.ToList();
        }
    }
}
