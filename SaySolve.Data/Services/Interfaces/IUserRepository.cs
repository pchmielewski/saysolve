﻿using System.Collections.Generic;
using SaySolve.Data.Models;

namespace SaySolve.Data.Services.Interfaces
{
    public interface IUserRepository
    {
        void AddUser(User user);
        IEnumerable<User> GetUsers();
    }
}
