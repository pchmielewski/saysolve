﻿using System;
using System.Windows.Forms;
using Microsoft.AspNet.SignalR.Client;

namespace SaySolve.Client.WinFormsClient
{
    public partial class Form1 : Form
    {
        private IHubProxy _chat;

        public Form1()
        {
            InitializeComponent();
        }

        private void connectBtn_Click(object sender, EventArgs e)
        {
            var hubConnection = new HubConnection("http://localhost:9997");

            _chat = hubConnection.CreateHubProxy("chat");
            _chat.On<string>("newMessage",msg=>messagesListBox.Invoke(new Action(() =>
            {
                messagesListBox.Items.Add(msg);
            })));

            // zrobic w pelni asynchroniczne z continue with 
            hubConnection.Start().Wait(); // to jest asynchroniczne 

            // zaimplementowac reszte 
            hubConnection.Reconnected += HubConnectionOnReconnected;
            hubConnection.Error += HubConnectionOnError;
        }

        private void HubConnectionOnError(Exception exception)
        {
            //
        }

        private void HubConnectionOnReconnected()
        {
            // dodac do jakiegos loggera
        }

        private void sendBtn_Click(object sender, EventArgs e)
        {
            _chat.Invoke<string>("sendMessage", messageTbx.Text);
        }
    }
}
