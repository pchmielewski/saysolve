﻿using System.Web.Mvc;
using SaySolve.Chat.Client.WebApp.Models;

namespace SaySolve.Chat.Client.WebApp.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index(Login login)
        {
            // jakies akcje etc 
            // wyswietlamy label z imieniem i z niego pobieramy do signalR
            return View(login);
        }

        public ActionResult Login()
        {
            return View();
        }
    }
}